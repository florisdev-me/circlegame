using Common.Unity;
using UnityEngine;

namespace Environment
{
    public class PowerUpSpawner : BetterMonoBehaviour
    {
        [SerializeField] private Level _level;

        protected override void InitReferences()
        {
            base.InitReferences();
            
            _level.RegisterPowerUpSpawner(transform);
        }
    }
}
