using System.Collections;
using Characters.NPC;
using Common.Unity;
using ScriptableObjects.Characters.NPC;
using ScriptableObjects.Environment;
using UnityEngine;

namespace Environment
{
    public class Spawner : BetterMonoBehaviour
    {
        [SerializeField] private float _range;


        public IEnumerator SpawnWave(EnemyWaveSO wave, Level level)
        {
            foreach (var spawnableEnemy in wave.EnemiesToSpawn)
            {
                SpawnEnemy(spawnableEnemy, level);

                var delayTimer = spawnableEnemy.PostSpawnDelay;

                while (delayTimer > 0)
                {
                    if (!IsPaused)
                        delayTimer -= Time.deltaTime;

                    yield return null;
                }
            }
        }

        private void SpawnEnemy(SpawnableEnemySO spawnable, Level level)
        {
            var positionOffset = Random.insideUnitCircle * _range;
            var position = transform.position + positionOffset.ToVector3();
            
            var instance = Instantiate(spawnable.EnemyPrefab, position, Quaternion.identity);
            if (!instance.TryGetInterface<IEnemy>(out var enemy)) return;
            
            enemy.SetStats(spawnable.Stats);
            enemy.RegisterLevel(level);
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = new Color(1, 0, 1, .5f);
            Gizmos.DrawSphere(transform.position, _range);
        }
    }
}