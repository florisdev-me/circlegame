using System;
using Common.Unity;
using ScriptableObjects.PowerUps;
using UnityEngine;

namespace Environment
{
    /// <summary>
    /// Script to manage everything related to the pickup object, associated to a power up
    /// </summary>
    public class PowerUpPickup : MonoBehaviour, IPowerUpPickup
    {
        private APowerUpSO _powerUp;
        private Action _onPickup;
        private SpriteRenderer _renderer; 
        
        public void SetupPickup(APowerUpSO powerUpSO, Action onPickup)
        {
            _renderer = GetComponent<SpriteRenderer>();
            
            _onPickup = onPickup;
            _powerUp = powerUpSO;
            _renderer.sprite = powerUpSO.Sprite;
        }

        public APowerUpSO GetPowerUp()
        {
            _onPickup();
            return _powerUp;
        }
    }

    public interface IPowerUpPickup
    {
        void SetupPickup(APowerUpSO powerUpSO, Action onPickup);
        APowerUpSO GetPowerUp();
    }
}
