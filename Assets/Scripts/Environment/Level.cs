using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common.Unity;
using Common.Unity.GameManager;
using Environment.LevelAnimations;
using GameSetup;
using ScriptableObjects.Environment;
using ScriptableObjects.GameSetup;
using ScriptableObjects.GameSetup.PowerUps;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace Environment
{
    public class Level : BetterMonoBehaviour
    {
        private int _enemyCount = 0;
        private PowerUpManagerSO _powerUpManager;

        [SerializeField] private Transform _playerSpawn;
        [SerializeField] private List<EnemyWaveSO> _waves;
        [SerializeField] private NavMeshData _navMeshData;

        private List<Spawner> _hostileSpawners;
        private List<ILevelAnimation> _levelAnimations;
        private int _finishedAnimations;
        private List<Transform> _powerUpSpawners;
        private List<GameObject> _spawnedPickups;

        private Action _onLevelFinished;

        private void OnEnable()
        {
            _hostileSpawners = new List<Spawner>();
            _powerUpSpawners = new List<Transform>();
            _levelAnimations = new List<ILevelAnimation>();
            _finishedAnimations = 0;
            _enemyCount = 0;

            foreach (var enemyWaveSO in _waves)
            {
                _enemyCount += enemyWaveSO.GetEnemyCount();
            }
        }

        protected override void InitReferences()
        {
            if (!GameLibrary.TryGetInstance(out var library))
                return;

            _powerUpManager = library.PowerUpManager;
            gameManager = library.GameManager;

            _levelAnimations = gameObject.GetInterfacesInChildren<ILevelAnimation>().ToList();
            _hostileSpawners = gameObject.GetComponentsInChildren<Spawner>().ToList();

            NavMesh.RemoveAllNavMeshData();
            NavMesh.AddNavMeshData(_navMeshData);
        }

        protected override void InitInvokers()
        {
            base.InitInvokers();
            
            if (_levelAnimations.Count == 0)
            {
                _onLevelFinished();
            }
            else
            {
                foreach (var levelAnimation in _levelAnimations)
                {
                    levelAnimation.PlayAnimation(LevelAnimationFinished);
                }
            }
        }

        private void LevelAnimationFinished()
        {
            _finishedAnimations++;

            if (_finishedAnimations < _levelAnimations.Count)
                return;
            
            foreach (var spawner in _hostileSpawners.OrderBy(_ => Random.Range(0f, 1f)))
            {
                var index = Mathf.Min(_waves.Count - 1, _hostileSpawners.IndexOf(spawner));
                StartCoroutine(spawner.SpawnWave(_waves[index], this));
            }

            _onLevelFinished();
        }

        public void RegisterLevelAnimation(ILevelAnimation levelAnimation)
        {
            _levelAnimations.Add(levelAnimation);
        }

        public void RegisterPowerUpSpawner(Transform location)
        {
            _powerUpSpawners.Add(location);
        }

        public void EnemyDied()
        {
            _enemyCount--;

            if (_enemyCount <= 0)
            {
                gameManager.LevelVictory();
            }
        }

        private void SpawnUpgrades()
        {
            if (_spawnedPickups != null)
                return;

            var powerUps = _powerUpManager.GetPotentialPowerUps(_powerUpSpawners.Count);
            _spawnedPickups = new List<GameObject>();

            for (var i = 0; i < _powerUpSpawners.Count; i++)
            {
                var powerUp = powerUps[i];
                var spawn = _powerUpSpawners[i];

                var instance = Instantiate(powerUp.PickupPrefab, spawn.position, Quaternion.identity);
                _spawnedPickups.Add(instance);

                if (instance.TryGetInterface<IPowerUpPickup>(out var pickup))
                {
                    pickup.SetupPickup(powerUp, PickedPowerUp);
                }
            }
        }

        private void PickedPowerUp()
        {
            foreach (var pickup in _spawnedPickups.Where(pickup => pickup != null))
            {
                Destroy(pickup);
            }

            gameManager.PickedLevelReward();
        }

        public void InitializeLevel(Action onComplete, UnityAction<Vector2> transitionPlayerSpawn)
        {
            transitionPlayerSpawn(_playerSpawn.position);
            _onLevelFinished = onComplete;
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            Destroy(gameObject);
        }

        protected override void OnGameStateChanged()
        {
            base.OnGameStateChanged();

            if (gameManager.StateMachine.ActiveState == GameState.LevelRewardSelection)
            {
                SpawnUpgrades();
            }
        }
    }
}