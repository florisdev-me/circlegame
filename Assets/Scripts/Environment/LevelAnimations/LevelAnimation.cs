﻿using System;
using System.Collections;
using Common.Unity;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Environment.LevelAnimations
{
    [RequireComponent(typeof(DOTweenAnimation))]
    public class LevelAnimation : BetterMonoBehaviour, ILevelAnimation
    {
        [SerializeField] private Level _level;
        private DOTweenAnimation _animation;
        private Action _onComplete;

        private void OnEnable()
        {
            _animation = GetComponent<DOTweenAnimation>();
            _animation.onComplete.AddListener(CompleteAnimation);
        }

        protected override void InitReferences()
        {
            base.InitReferences();

            _level.RegisterLevelAnimation(this);
        }

        protected override void TogglePause()
        {
            
        }

        public void PlayAnimation(Action onComplete)
        {
            _onComplete = onComplete;

            Debug.Log("Play");
            _animation.DOPlay();
            // StartCoroutine(DelayAndPlay(Random.Range(0, 10f)));
        }

        private IEnumerator DelayAndPlay(float delay)
        {
            yield return new WaitForSeconds(delay);
            _animation.DOPlay();
        }

        private void CompleteAnimation()
        {
            _onComplete();
        }
    }
}