﻿using System;
using System.Collections;
using Common.Unity;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Environment.LevelAnimations
{
    public class ScaleLevelAnimation : BetterMonoBehaviour, ILevelAnimation
    {
        [SerializeField] private Ease _easeType = Ease.OutBack;
        [SerializeField] private Vector3 _startScale = Vector3.zero;
        
        [SerializeField] private float _duration = .5f;
        private float _maxDelay = .5f;
        
        private Vector3 _finalScale;

        private void OnEnable()
        {
            _finalScale = transform.localScale;
            transform.localScale = _startScale;
        }

        protected override void TogglePause()
        {
        }

        public void PlayAnimation(Action onComplete)
        {
            StartCoroutine(DelayAndPlay(Random.Range(0, _maxDelay), onComplete));
        }

        private IEnumerator DelayAndPlay(float delay, Action onComplete)
        {
            yield return new WaitForSeconds(delay);
            
            transform.DOScale(_finalScale, _duration).SetEase(_easeType).OnComplete(
                () => onComplete()
            );
        }
    }
}