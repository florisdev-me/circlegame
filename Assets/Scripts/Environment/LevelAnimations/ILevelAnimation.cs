﻿using System;

namespace Environment.LevelAnimations
{
    public interface ILevelAnimation
    {
        void PlayAnimation(Action onComplete);
    }
}