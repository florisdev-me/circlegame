﻿using UnityEngine;

namespace Combat
{
    public class HitInfo
    {
        public float Damage { get; }
        public Vector2 Direction { get; }
        public float StunDuration { get; }
        public float KnockBack { get; }

        public HitInfo(float damage, Vector2 direction, float stunDuration = 1, float knockBack = 10)
        {
            Damage = damage;
            Direction = direction;
            StunDuration = stunDuration;
            KnockBack = knockBack;
        }
    }
}