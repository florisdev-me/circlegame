﻿using UnityEngine;

namespace Combat
{
    public interface IDamageable
    {
        public void TakeHit(HitInfo hitInfo);
    }
}