using System;
using Common.Unity;
using ScriptableObjects.GameSetup.PowerUps;
using UnityEngine;

namespace Combat
{
    public class Projectile : BetterMonoBehaviour
    {
        private Rigidbody2D _rigidbody;
        private float _lifeTime = 5f;
        private float _speed = 10f;
        private float _baseDamage = 1f;
        private Vector2 _direction;

        private void OnEnable()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        public void SetupProjectile(Vector2 direction)
        {
            _direction = direction;
        }
        
        private void FixedUpdate()
        {
            if (IsPaused) return;

            if (_direction == Vector2.zero)
            {
                return;
            }
            
            if (_lifeTime <= 0)
            {
                Destroy(gameObject);
            }

            _lifeTime -= Time.fixedDeltaTime;
            
            // Move logic
            var targetPosition = CalculateMove(_rigidbody.position);
            _rigidbody.MovePosition(targetPosition);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            Bounce(other);
            
            if (!other.gameObject.TryGetInterface<IDamageable>(out var damageable))
                return;

            var damageMultiplier = gameLibrary.PowerUpManager.PlayerPowerUpManagerSO.GetPowerUpValue(PlayerStatType.DamageMultiplier);
            damageable.TakeHit(new HitInfo(damageMultiplier * _baseDamage, Vector2.zero, 0, 0));
            
            Destroy(gameObject);
        }


        private Vector2 CalculateMove(Vector2 sourcePosition)
        {
            return sourcePosition + _direction * _speed * Time.fixedDeltaTime;
        }
        
        private void Bounce(Collision2D collision)
        {
            if (collision.contactCount < 1)
            {
                return;
            }
            
            var firstContact = collision.contacts[0];
            
            // R = 𝐷 − 2(𝐷 ∙ 𝑁)N
            var reflected = _direction - (Vector2)(2 * Vector2.Dot(_direction, firstContact.normal) * firstContact.normal);
            
            _direction = reflected;
        }
    }
}