using System;
using System.Collections.Generic;
using Common.Unity;
using Common.Unity.GameManager;
using ScriptableObjects.Environment;
using UnityEngine;

namespace GUI.Menus
{
    public class NextLevelSelect : BetterMonoBehaviour
    {
        [SerializeField] private GameObject _levelButtonPrefab;

        private List<GameObject> _buttonList;

        protected override void OnGameStateChanged()
        {
            if (gameManager.StateMachine.ActiveState != GameState.LevelSelection)
                return;

            _buttonList = new List<GameObject>();
            foreach (var roomOption in gameManager.LevelManager.GetLevelCandidates())
            {
                var instance = Instantiate(_levelButtonPrefab, this.transform, true);
                _buttonList.Add(instance);
                if (instance.TryGetInterface<ILevelButton>(out var roomButton))
                {
                    roomButton.SetLevelSO(roomOption, () => LevelSelected(roomOption));
                }
            }
        }

        private void LevelSelected(LevelSO level)
        {
            // Clean list
            foreach (var button in _buttonList)
            {
                Destroy(button.gameObject);
            }
            
            gameManager.StartNextLevel(level);
        }
    }
}
