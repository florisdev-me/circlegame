using System;
using ScriptableObjects.Environment;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GUI.Menus
{
    public class LegacyLevelButton : MonoBehaviour, ILevelButton
    {
        private Action _onClick;
        private Button _button;
        private Image _image;
        
        public void SetLevelSO(LevelSO level, Action onClick)
        {
            _button = GetComponent<Button>();
            _image = GetComponent<Image>();
            
            _onClick = onClick;

            _image.sprite = level.Sprite;
            _button.onClick.AddListener(OnClick);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            _onClick();
        }
    }
}
