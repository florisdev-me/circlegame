using System;
using ScriptableObjects.Environment;
using UnityEngine;

namespace GUI.Menus
{
    public interface ILevelButton
    {
        void SetLevelSO(LevelSO level, Action onClick);
    }
}
