using System;
using UnityEngine;
using Common.Unity;
using Common.Unity.GameManager;
using TMPro;

namespace GUI.Debug
{
    public class DisplayGameState : BetterMonoBehaviour
    {
        private TMP_Text _text;

        private void OnEnable()
        {
            _text = GetComponent<TMP_Text>();
        }

        protected override void OnGameStateChanged()
        {
            _text.text = gameManager.StateMachine.ActiveState.ToString();
        }
    }
}