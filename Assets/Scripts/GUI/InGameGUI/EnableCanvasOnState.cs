using System;
using Common.Unity;
using Common.Unity.GameManager;
using UnityEngine;

namespace GUI.InGameGUI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class EnableCanvasOnState : BetterMonoBehaviour
    {
        [SerializeField] private GameState _gameState;
        private CanvasGroup _canvas;

        private void OnEnable()
        {
            _canvas = GetComponent<CanvasGroup>();
        }

        protected override void OnGameStateChanged()
        {
            if (gameManager.StateMachine.ActiveState != _gameState)
            {
                _canvas.alpha = 0;
                _canvas.interactable = false;
                _canvas.blocksRaycasts = false;
            }
            else
            {
                _canvas.alpha = 1;
                _canvas.interactable = true;
                _canvas.blocksRaycasts = true;
            }
        }
    }
}
