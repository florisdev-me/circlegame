using System;
using Common.Unity.GameManager;
using UnityEngine;

namespace GameSetup
{
    public class CameraGuide : MonoBehaviour
    {
        [SerializeField] private Transform _guide1;
        [SerializeField] private Transform _guide2;
        [SerializeField] private float _maxDistance;

        private void FixedUpdate()
        {
            if (_guide1 == null || _guide2 == null)
                return;

            var direction = (_guide1.position - _guide2.position) / 2;
            transform.position = _guide2.position + direction.normalized * Math.Min(_maxDistance, direction.magnitude);
        }
    }
}
