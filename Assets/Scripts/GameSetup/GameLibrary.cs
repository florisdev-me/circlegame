﻿using System;
using System.Collections;
using Common.Unity;
using Common.Unity.GameManager;
using ScriptableObjects.GameSetup;
using ScriptableObjects.GameSetup.PowerUps;
using UnityEngine;
using UnityEngine.AI;

namespace GameSetup
{
    public class GameLibrary : UnitySingleton<GameLibrary>
    {
        [SerializeField] private InputHandler _inputHandler;
        public InputHandler InputHandler => _inputHandler;
        
        [SerializeField] private GameManagerSO _gameManager;
        public GameManagerSO GameManager => _gameManager;
        
        [SerializeField] private MouseDataSO _mouseData;
        public MouseDataSO MouseData => _mouseData;

        [SerializeField] private PowerUpManagerSO _powerUpManager;
        public PowerUpManagerSO PowerUpManager => _powerUpManager;
        
        [SerializeField] private NavMeshDataInstance  _navMeshSurface;
        public NavMeshDataInstance  NavMeshSurface => _navMeshSurface;
        
        private void OnEnable()
        {
            if (InputHandler == null)
                Debug.LogError("InputHandler not assigned");
            
            if (GameManager == null)
                Debug.LogError("GameManager not assigned");
            
            if (MouseData == null)
                Debug.LogError("MouseData not assigned");
            
            if (PowerUpManager == null)
                Debug.LogError("UpgradeManager not assigned");
            
            StartCoroutine(WaitForInitialization(() => _gameManager.SceneFinishedLoading()));
        }

        /// <summary>
        /// Wait for all BetterMonobehaviour functions to be called
        /// </summary>
        private IEnumerator WaitForInitialization(Action onComplete)
        {
            yield return new WaitForEndOfFrame();
            // InitReferences();

            yield return new WaitForEndOfFrame();
            // InitListeners();

            yield return new WaitForEndOfFrame();
            // InitInvokers();

            yield return new WaitForEndOfFrame();
            onComplete();
        }

    }
}