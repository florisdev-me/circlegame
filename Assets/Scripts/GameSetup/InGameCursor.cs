using ScriptableObjects.GameSetup;
using UnityEngine;

namespace GameSetup
{
    public class InGameCursor : MonoBehaviour
    {
        [SerializeField] private MouseDataSO _mouseData;
        
        private void FixedUpdate()
        {
            transform.position = _mouseData.WorldPosition;
        }
    }
}
