﻿using System;
using System.Collections.Generic;
using UnityEngine.Events;

namespace Common.FiniteStateMachine
{
    public class FiniteStateMachine<TStates, TOwner>
    {
        private readonly Dictionary<TStates, BaseState<TOwner>> _states;
        private BaseState<TOwner> CurrentState { get; set; }
        public TStates ActiveState { get; private set; }
        public UnityAction StateChanged = delegate { };

        public FiniteStateMachine(Dictionary<TStates, BaseState<TOwner>> states)
        {
            _states = states;
        }

        public void ChangeState(TStates nextState)
        {
            if (ActiveState.Equals(nextState))
            {
                return;
            }
            
            if (!_states.ContainsKey(nextState))
            {
                throw new ArgumentOutOfRangeException(nameof(nextState),"State was not found in the states dictionary");
            }

            CurrentState?.Exit();
            ActiveState = nextState;
            CurrentState = _states[nextState];
            CurrentState?.Enter();
            StateChanged.Invoke();
        }

        public void FixedUpdate()
        {
            CurrentState?.FixedUpdate();
        }

        public void Cleanup()
        {
            CurrentState?.Exit();
        }
    }
}