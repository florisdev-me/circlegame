﻿namespace Common.FiniteStateMachine
{
    public abstract class BaseState<TOwner> : IState
    {
        protected readonly TOwner owner;

        protected BaseState(TOwner owner)
        {
            this.owner = owner;
        }

        public virtual void Enter()
        {
            
        }

        public virtual void Exit()
        {
            
        }

        public virtual void FixedUpdate()
        {
            
        }

        public virtual void Update()
        {
            
        }
    }
}