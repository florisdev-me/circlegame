﻿using System;
using UnityEngine;

namespace Common.Test
{
    [CreateAssetMenu(fileName = "TestSO", menuName = "Game Setup/Tests/Test SO")]
    public class TestSO : ScriptableObject
    {
        private void Awake()
        {
            Debug.Log("SO AWAKE");
        }

        private void OnEnable()
        {
            Debug.Log("SO ENABLED");
        }

        private void OnDisable()
        {
            Debug.Log("SO DISABLED");
        }
    }
}