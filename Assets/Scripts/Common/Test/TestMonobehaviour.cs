﻿using Common.Unity;
using UnityEngine;

namespace Common.Test
{
    public class TestMonobehaviour : BetterMonoBehaviour
    {
        [SerializeField] private TestSO _testSO;

        protected override void Awake()
        {
            Debug.Log("Mono AWAKE");
        }

        private void OnEnable()
        {
            Debug.Log("Mono ENABLED");
        }
        
        protected override void InitReferences()
        {
            Debug.Log("Mono InitReferences");
        }
        
        protected override void InitListeners()
        {
            Debug.Log("Mono InitListeners");
        }

        protected override void InitInvokers()
        {
            Debug.Log("Mono InitInvokers");
        }

        protected override void OnDisable()
        {
            Debug.Log("Mono DISABLED");
        }

        protected override void TogglePause()
        {
            Debug.Log($"Game paused: {IsPaused}");
        }

        private void Start()
        {
            Debug.Log("Mono STARTED");
        }
    }
}