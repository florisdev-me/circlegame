﻿using System;
using System.Collections;
using Common.Unity.GameManager;
using DG.Tweening;
using GameSetup;
using UnityEngine;

namespace Common.Unity
{
    /// <summary>
    /// Overwrite of monobehaviour, to ensure execution order of object initialization
    /// </summary>
    public abstract class BetterMonoBehaviour : MonoBehaviour
    {
        protected GameManagerSO gameManager;
        protected GameLibrary gameLibrary;

        protected bool IsPaused { get; private set; } = false;

        protected virtual void Awake()
        {
            StartCoroutine(SetupBetterMonoBehaviour());
        }

        private IEnumerator SetupBetterMonoBehaviour()
        {
            // Wait a frame so every Awake/Start/OnEnable method is called
            yield return new WaitForEndOfFrame();
            InitReferences();

            yield return new WaitForEndOfFrame();
            InitListeners();

            yield return new WaitForEndOfFrame();
            InitInvokers();
        }

        /// <summary>
        /// Used to initialize all references
        /// </summary>
        protected virtual void InitReferences()
        {
            if (!GameLibrary.TryGetInstance(out gameLibrary))
                return;

            gameManager = gameLibrary.GameManager;
            IsPaused = gameManager.IsPaused;
        }

        /// <summary>
        /// Used to initialize all listeners
        /// </summary>
        protected virtual void InitListeners()
        {
            gameManager.StateMachine.StateChanged += OnGameStateChanged;
        }

        /// <summary>
        /// Used to setup internal state, which impacts other classes.
        /// i.e. Invokes of events
        /// </summary>
        protected virtual void InitInvokers()
        {
        }

        protected virtual void OnDisable()
        {
            gameManager.StateMachine.StateChanged -= OnGameStateChanged;

            transform.DORewind();
            transform.DOKill();
        }

        protected virtual void OnGameStateChanged()
        {
            TogglePause();
        }

        protected virtual void TogglePause()
        {
            IsPaused = gameManager.IsPaused;

            if (IsPaused)
            {
                transform.DOPause();
            }
            else
            {
                transform.DOPlay();
            }
        }
    }
}