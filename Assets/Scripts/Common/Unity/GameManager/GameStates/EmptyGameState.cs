﻿using Common.FiniteStateMachine;

namespace Common.Unity.GameManager.GameStates
{
    public class EmptyGameState : BaseState<GameManagerSO>
    {
        public EmptyGameState(GameManagerSO owner) : base(owner)
        {
        }
    }
}