﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Common.Unity.GameManager
{
    [CreateAssetMenu(fileName = "SceneData", menuName = "Game Setup/Scene Data")]
    public class SceneDataSO : ScriptableObject
    {
        [SerializeField] private string _sceneName;

        public IEnumerator LoadSceneAsync()
        {
            // Start loading the scene
            var asyncLoadLevel = SceneManager.LoadSceneAsync(_sceneName, LoadSceneMode.Single);
            
            // Wait until the level finish loading
            while (!asyncLoadLevel.isDone)
                yield return null;
        }
    }
}