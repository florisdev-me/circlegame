using System;
using System.Collections;
using System.Collections.Generic;
using Common.FiniteStateMachine;
using Common.Unity.GameManager.GameStates;
using GameSetup;
using ScriptableObjects.Environment;
using ScriptableObjects.GameSetup;
using UnityEngine;

namespace Common.Unity.GameManager
{
    public enum GameState
    {
        None = 0,
        Playing = 1,
        Paused = 2,
        LoadingScene = 3,
        LoadingLevel = 4,
        GameOver = 5,
        LevelRewardSelection = 6,
        LevelSelection = 7,
        Victory = 8
    }

    [CreateAssetMenu(fileName = "GameManager", menuName = "Game Setup/Game Manager")]
    public class GameManagerSO : ScriptableObject
    {
        public FiniteStateMachine<GameState, GameManagerSO> StateMachine { get; private set; }
        [SerializeField] private LevelManagerSO _levelManager;
        public LevelManagerSO LevelManager => _levelManager;

        public bool IsPaused =>
            StateMachine.ActiveState != GameState.Playing &&
            StateMachine.ActiveState != GameState.LevelRewardSelection;

        private void OnEnable()
        {
            InitializeStateMachine();
            StateMachine.ChangeState(GameState.LoadingScene);
        }

        private void InitializeStateMachine()
        {
            var states = new Dictionary<GameState, BaseState<GameManagerSO>>()
            {
                { GameState.Playing, new EmptyGameState(this) },
                { GameState.Paused, new EmptyGameState(this) },
                { GameState.LoadingScene, new EmptyGameState(this) },
                { GameState.LevelRewardSelection, new EmptyGameState(this) },
                { GameState.LevelSelection, new EmptyGameState(this) },
                { GameState.LoadingLevel, new EmptyGameState(this) },
                { GameState.Victory, new EmptyGameState(this) },
                { GameState.GameOver, new EmptyGameState(this) }
            };

            StateMachine = new FiniteStateMachine<GameState, GameManagerSO>(states);
        }

        public void StartLevel(SceneDataSO sceneData)
        {
            StateMachine.ChangeState(GameState.LoadingScene);
            UnityHelper.Instance.StartCoroutine(sceneData.LoadSceneAsync());
        }

        public void SceneFinishedLoading()
        {
            if (!Application.isPlaying)
                return;

            StateMachine.ChangeState(GameState.LoadingLevel);
            LevelManager.LoadInitialLevel(() => StateMachine.ChangeState(GameState.Playing));
        }

        public void StartNextLevel(LevelSO nextLevel)
        {
            StateMachine.ChangeState(GameState.LoadingLevel);
            LevelManager.StartNextLevel(nextLevel, () => StateMachine.ChangeState(GameState.Playing));
        }

        public void PauseGame()
        {
            StateMachine.ChangeState(GameState.Paused);
        }

        public void UnPauseGame()
        {
            StateMachine.ChangeState(GameState.Playing);
        }

        public void GameOver()
        {
            StateMachine.ChangeState(GameState.GameOver);
        }

        public void LevelVictory()
        {
            StateMachine.ChangeState(GameState.LevelRewardSelection);
        }

        public void PickedLevelReward()
        {
            StateMachine.ChangeState(GameState.LevelSelection);
        }

        public void Victory()
        {
            StateMachine.ChangeState(GameState.Victory);
        }
    }
}