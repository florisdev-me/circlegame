﻿using System.Collections;
using Common.Unity.GameManager;
using UnityEngine;

namespace Common.Unity
{
    /// <summary>
    /// Helper class for any class that requires Unity/Monobehaviour functionality, but has no direct access
    /// </summary>
    public class UnityHelper : UnitySingleton<UnityHelper>
    {
        
    }
}