﻿using UnityEngine;
using System.Collections.Generic;

namespace Common.Unity
{
    public static class UnityLayerMasks
    {
        private static Dictionary<int, int> _masksByLayer;

        private static Dictionary<int, int> GenerateDict()
        {
            var masksByLayer = new Dictionary<int, int>();
            for (var i = 0; i < 32; i++)
            {
                var mask = 0;
                for (var j = 0; j < 32; j++)
                {
                    if (!Physics.GetIgnoreLayerCollision(i, j))
                    {
                        mask |= 1 << j;
                    }
                }

                masksByLayer.Add(i, mask);
            }

            return masksByLayer;
        }

        public static int MaskForLayer(int layer)
        {
            _masksByLayer ??= GenerateDict();
            return _masksByLayer[layer];
        }
    }
}