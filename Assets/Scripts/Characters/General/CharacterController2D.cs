using System;
using Common.Unity;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Characters.General
{
    /// <summary>
    /// Character controller for 2D top-down games
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D))]
    public class CharacterController2D : BetterMonoBehaviour
    {
        [Header("Regular movement")] 
        [SerializeField] private float _speed = 5f;
        [SerializeField] private float _traction = .1f;

        [Header("Dashing")] 
        [SerializeField] private float _dashDistance = 50f;
        [SerializeField] private float _dashDecay = 10f;
        [SerializeField] private float _dashThreshold = 5;
        public UnityAction FinishedDash = delegate {  };

        // Movement variables
        private Rigidbody2D _rigidbody;
        private Vector2 _inputDirection = Vector2.zero;
        private Vector2 _moveDirection = Vector2.zero;
        private Vector2 _dashVelocity = Vector2.zero;

        // Collision variables: For preventing the player from clipping through thin walls
        [Header("Collision detection")] [SerializeField]
        private LayerMask _collisionLayerMask = default;

        private void OnEnable()
        {
            // Get Components
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        private void FixedUpdate()
        {
            if (IsPaused)
                return;

            // Move logic
            var targetPosition = CalculateMove(_rigidbody.position);
            targetPosition = CalculateDash(targetPosition);

            var direction = targetPosition - _rigidbody.position;
            var collisionRay = new Ray(_rigidbody.position, direction);
            if (!Physics.Raycast(collisionRay, out _, direction.magnitude, _collisionLayerMask))
            {
                _rigidbody.MovePosition(targetPosition);
            }
        }

        private Vector2 CalculateMove(Vector2 sourcePosition)
        {
            var inputDirection = _inputDirection;
            _moveDirection = Vector2.Lerp(_moveDirection, inputDirection, _traction);

            return sourcePosition + _moveDirection * (_speed * Time.fixedDeltaTime);
        }

        private Vector2 CalculateDash(Vector2 sourcePosition)
        {
            // Dash logic
            if (_dashVelocity == Vector2.zero)
            {
                return sourcePosition;
            }

            sourcePosition += _dashVelocity * Time.fixedDeltaTime;
            _dashVelocity = Vector2.Lerp(_dashVelocity, Vector2.zero, _dashDecay * Time.fixedDeltaTime);

            if (_dashVelocity.magnitude < _dashThreshold)
            {
                FinishedDash.Invoke();
                _dashVelocity = Vector2.zero;
            }

            return sourcePosition;
        }

        #region Input
        public void MoveInDirection(Vector2 direction)
        {
            _inputDirection = direction;
        }

        public void Dash(Vector2 direction, bool overrideDirection, float multiplier = 1)
        {
            if (_inputDirection == Vector2.zero || overrideDirection)
            {
                _dashVelocity = direction * _dashDistance * multiplier;
            }
            else
            {
                _dashVelocity = _inputDirection * _dashDistance * multiplier;
            }
        }
        
        public void Reset()
        {
            _moveDirection = Vector2.zero;
            _inputDirection = Vector2.zero;
            _dashVelocity = Vector2.zero;
        }
        #endregion

        #region Debug

        private void OnDrawGizmosSelected()
        {
            ExtraGizmos.DrawArrow(transform.position, _inputDirection);
        }

        #endregion
    }
}