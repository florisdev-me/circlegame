﻿using System.Collections.Generic;
using Characters.General;
using Common.FiniteStateMachine;
using GameSetup;
using ScriptableObjects.GameSetup;
using UnityEngine;

namespace Characters.Player.States
{
    public class PlayerDashingState : MonobehaviourState<Player>
    {
        private CharacterController2D _controller;

        public PlayerDashingState(Player player) : base(player)
        {
            _controller = owner.Controller;
        }

        public override void Enter()
        {
            var direction = (owner.MouseData.WorldPosition - transform.position).normalized;
            _controller.Dash(direction, owner.DashMouseDirection);

            // Events
            owner.InputHandler.MoveEvent += OnMove;
            _controller.FinishedDash += FinishDash;
        }

        public override void Exit()
        {
            _controller.FinishedDash -= FinishDash;
            owner.InputHandler.MoveEvent -= OnMove;
        }

        private void OnMove(Vector2 direction)
        {
            _controller.MoveInDirection(direction);
        }

        private void FinishDash()
        {
            owner.StateMachine.ChangeState(PlayerStates.Moving);
        }
    }
}