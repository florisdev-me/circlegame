﻿using Characters.General;
using Common.FiniteStateMachine;
using GameSetup;
using ScriptableObjects.GameSetup;
using UnityEngine;

namespace Characters.Player.States
{
    public class PlayerMovementState : MonobehaviourState<Player>
    {
        private CharacterController2D _controller;

        public PlayerMovementState(Player player) : base(player)
        {
            _controller = owner.Controller;
        }

        public override void Enter()
        {
            owner.InputHandler.MoveEvent += OnMove;
        }

        public override void Exit()
        {
            owner.InputHandler.MoveEvent -= OnMove;
        }

        private void OnMove(Vector2 direction)
        {
            _controller.MoveInDirection(direction);
        }
    }
}