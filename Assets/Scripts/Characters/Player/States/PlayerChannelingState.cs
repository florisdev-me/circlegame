﻿using Common.FiniteStateMachine;

namespace Characters.Player.States
{
    public class PlayerChannelingState : MonobehaviourState<Player>
    {
        public PlayerChannelingState(Player player) : base(player)
        {
        }
    }
}