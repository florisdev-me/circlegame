﻿using Common.FiniteStateMachine;
using Common.Unity;
using DG.Tweening;

namespace Characters.Player.States
{
    public class PlayerLoadingNewLevelState : MonobehaviourState<Player>
    {
        public PlayerLoadingNewLevelState(Player owner) : base(owner)
        {
        }

        public override void Enter()
        {
            base.Enter();
            DOTween.To(() => owner.transform.position, x => owner.transform.position = x.ToVector3(), owner.SpawnPoint,
                .5f).OnComplete(() => owner.OnSpawnTransitionComplete());
        }
    }
}