using System;
using System.Collections.Generic;
using Characters.General;
using Characters.Player.States;
using Common.FiniteStateMachine;
using Common.Unity;
using Common.Unity.GameManager;
using GameSetup;
using ScriptableObjects.Characters.Player;
using ScriptableObjects.GameSetup;
using ScriptableObjects.PowerUps;
using UnityEngine;

namespace Characters.Player
{
    public enum PlayerStates
    {
        None,
        Moving,
        Dashing,
        Channeling,
        LoadingNewLevel
    }

    [RequireComponent(typeof(CharacterController2D))]
    public class Player : BetterMonoBehaviour
    {
        public FiniteStateMachine<PlayerStates, Player> StateMachine { get; private set; }
        
        [SerializeField] private PlayerStatsSO _stats;
        public PlayerStatsSO Stats => _stats;

        [SerializeField] private bool _dashMouseDirection;
        public bool DashMouseDirection => _dashMouseDirection;
        public InputHandler InputHandler { get; private set; }
        public MouseDataSO MouseData { get; private set; }
        public CharacterController2D Controller { get; private set; }
        public Vector2 SpawnPoint { get; private set; }
        
        private void OnEnable()
        {
            Controller = GetComponent<CharacterController2D>();
            InitializeStateMachine();
        }

        protected override void InitReferences()
        {
            base.InitReferences();
            
            InputHandler = gameLibrary.InputHandler;
            MouseData = gameLibrary.MouseData;
            gameLibrary.PowerUpManager.PlayerPowerUpManagerSO.RegisterPlayer(this);
        }

        protected override void InitListeners()
        {
            base.InitListeners();

            gameManager.LevelManager.TransitionPlayerSpawn += OnTransitionNewSpawn;
        }

        protected override void InitInvokers()
        {
            StateMachine.ChangeState(PlayerStates.Moving);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            gameManager.LevelManager.TransitionPlayerSpawn -= OnTransitionNewSpawn;
            StateMachine.Cleanup();
        }

        private void InitializeStateMachine()
        {
            var stateDict = new Dictionary<PlayerStates, BaseState<Player>>
            {
                { PlayerStates.Moving, new PlayerMovementState(this) },
                { PlayerStates.Dashing, new PlayerDashingState(this) },
                { PlayerStates.Channeling, new PlayerChannelingState(this) },
                { PlayerStates.LoadingNewLevel, new PlayerLoadingNewLevelState(this) }
            };

            StateMachine = new FiniteStateMachine<PlayerStates, Player>(stateDict);
        }

        private void OnTransitionNewSpawn(Vector2 newSpawnPoint)
        {
            SpawnPoint = newSpawnPoint;
            Controller.Reset();
            StateMachine.ChangeState(PlayerStates.LoadingNewLevel);
        }

        public void OnSpawnTransitionComplete()
        {
            gameManager.LevelManager.PlayerFinishedMoving();
            StateMachine.ChangeState(PlayerStates.Moving);
        }
    }
}