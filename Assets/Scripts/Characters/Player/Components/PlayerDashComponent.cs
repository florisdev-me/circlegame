﻿using System;
using System.Collections.Generic;
using Characters.General;
using Common.Unity;
using GameSetup;
using ScriptableObjects.GameSetup;
using UnityEngine;

namespace Characters.Player.Components
{
    public class PlayerDashComponent : BetterMonoBehaviour
    {
        private List<PlayerStates> _activeStates;

        private Player _player;

        private void OnEnable()
        {
            _activeStates = new List<PlayerStates>()
            {
                PlayerStates.Dashing,
                PlayerStates.Moving
            };

            _player = GetComponent<Player>();
        }
        
        protected override void InitListeners()
        {
            base.InitListeners();
            
            gameLibrary.InputHandler.DashEvent += OnDash;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            gameLibrary.InputHandler.DashEvent -= OnDash;
        }

        private void OnDash(bool pressed)
        {
            if (!_activeStates.Contains(_player.StateMachine.ActiveState) || IsPaused) return;
            if (!pressed) return;

            _player.StateMachine.ChangeState(PlayerStates.Dashing);
        }
    }
}