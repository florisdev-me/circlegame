﻿using System.Collections.Generic;
using Combat;
using Common.Unity;
using UnityEngine;

namespace Characters.Player.Components
{
    public class PlayerRangedAttackComponent : BetterMonoBehaviour
    {
        private List<PlayerStates> _activeStates;
        [SerializeField] private GameObject _attack;
        private Player _player;

        private void OnEnable()
        {
            _activeStates = new List<PlayerStates>()
            {
                PlayerStates.Moving
            };

            _player = GetComponent<Player>();
        }
        
        protected override void InitListeners()
        {
            base.InitListeners();
            
            gameLibrary.InputHandler.FireEvent += OnFire;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            gameLibrary.InputHandler.FireEvent -= OnFire;
        }

        private void OnFire(bool pressed)
        {
            if (!_activeStates.Contains(_player.StateMachine.ActiveState) || IsPaused) return;
            if (!pressed || _attack == null) return;

            var direction = (gameLibrary.MouseData.WorldPosition - transform.position).normalized;
            
            var instance = Instantiate(_attack, transform.position + direction * transform.localScale.x, transform.localRotation);
            instance.GetComponent<Projectile>().SetupProjectile(direction);
        }

        public void SetAttack(GameObject attack)
        {
            _attack = attack;
        }
    }
}