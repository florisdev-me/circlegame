﻿using System.Collections.Generic;
using Common.Unity;
using Environment;
using ScriptableObjects.PowerUps;
using UnityEngine;

namespace Characters.Player.Components
{
    public class PlayerPowerUpPickup : BetterMonoBehaviour
    {
        [SerializeField] private List<PlayerStates> _collectInStates;
        private Player _player;
        private bool _pickedUp = false;

        private void OnEnable()
        {
            _player = GetComponent<Player>();
        }

        protected override void InitListeners()
        {
            base.InitInvokers();
            
            _player.StateMachine.StateChanged += ResetPickup;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            _player.StateMachine.StateChanged -= ResetPickup;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!_collectInStates.Contains(_player.StateMachine.ActiveState)) return;
            if (!other.gameObject.TryGetInterface<IPowerUpPickup>(out var powerUpPickup)) return;

            GrabPowerUp(powerUpPickup);
        }

        /// <summary>
        /// Damage already overlapping enemies, since they won't fire on trigger enter
        /// </summary>
        private void OnTriggerExit2D(Collider2D other)
        {
            if (!_collectInStates.Contains(_player.StateMachine.ActiveState)) return;
            if (!other.gameObject.TryGetInterface<IPowerUpPickup>(out var powerUpPickup)) return;

            GrabPowerUp(powerUpPickup);
        }

        private void GrabPowerUp(IPowerUpPickup powerUpPickup)
        {
            if (_pickedUp)
                return;
            
            _pickedUp = true;
            var powerUp = powerUpPickup.GetPowerUp();
            gameLibrary.PowerUpManager.ApplyPowerUp(powerUp);
        }

        protected override void OnGameStateChanged()
        {
            base.OnGameStateChanged();
            _pickedUp = false;
        }

        private void ResetPickup()
        {
            _pickedUp = false;
        }
    }
}