using System;
using System.Collections.Generic;
using Characters.NPC;
using Combat;
using Common.Unity;
using ScriptableObjects.GameSetup.PowerUps;
using UnityEngine;

namespace Characters.Player.Components
{
    public class PlayerDamageOnImpactComponent : BetterMonoBehaviour
    {
        [SerializeField] private List<PlayerStates> _activeStates = new List<PlayerStates>();

        private List<IDamageable> _alreadyHit = new();
        private Player _player;
        private PlayerPowerUpManagerSO _powerUpManager;

        private void OnEnable()
        {
            _player = GetComponent<Player>();
        }

        protected override void InitReferences()
        {
            base.InitReferences();

            _powerUpManager = gameLibrary.PowerUpManager.PlayerPowerUpManagerSO;
        }

        protected override void InitListeners()
        {
            base.InitListeners();
            
            _player.StateMachine.StateChanged += () => PlayerStateChange(_player.StateMachine.ActiveState);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            _player.StateMachine.StateChanged -= () => PlayerStateChange(_player.StateMachine.ActiveState);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!_activeStates.Contains(_player.StateMachine.ActiveState)) return;
            if (!other.gameObject.TryGetInterface<IDamageable>(out var damageable)) return;
            if (_alreadyHit.Contains(damageable)) return;

            DamageEnemy(other, damageable);
        }

        /// <summary>
        /// Damage already overlapping enemies, since they won't fire on trigger enter
        /// </summary>
        private void OnTriggerExit2D(Collider2D other)
        {
            if (!_activeStates.Contains(_player.StateMachine.ActiveState)) return;
            if (!other.gameObject.TryGetInterface<IDamageable>(out var damageable)) return;
            if (_alreadyHit.Contains(damageable)) return;

            DamageEnemy(other, damageable);
        }
        
        private void PlayerStateChange(PlayerStates newState)
        {
            ResetList(newState);
        }

        private void ResetList(PlayerStates newState)
        {
            if (!_activeStates.Contains(newState)) return;

            _alreadyHit = new List<IDamageable>();
        }

        private void DamageEnemy(Component enemy, IDamageable damageable)
        {
            if (_alreadyHit.Contains(damageable))
                return;

            _alreadyHit.Add(damageable);
            
            var direction = enemy.transform.position - transform.position;

            var damageMultiplier = _powerUpManager.GetPowerUpValue(PlayerStatType.DashDamage);
            var damage = _player.Stats.DashBaseDamage * damageMultiplier;
            
            var stunDuration = _player.Stats.DashBaseStunDuration;
            var knockBack = _player.Stats.DashBaseKnockBack;
            
            var hitInfo = new HitInfo(damage, direction, stunDuration, knockBack);
            damageable.TakeHit(hitInfo);
        }

        public void AddToActiveStates(PlayerStates state)
        {
            if (!_activeStates.Contains(state)) _activeStates.Add(state);
        }
    }
}