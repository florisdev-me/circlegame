using System;
using Shapes;
using UnityEngine;
using DG.Tweening;

namespace Characters.NPC
{
    public class BasicAIHealthBar : ImmediateModeShapeDrawer
    {
        [SerializeField] private float _radius = .5f;
        [SerializeField] private float _thickness = .2f;

        [SerializeField] private BasicAI _basicAI;

        [SerializeField] [Range(0,1)]
        private float _fill = .5f;


        public override void OnEnable()
        {
            base.OnEnable();
            
            _fill = 1;
            _basicAI.HitAction += UpdateRatio;
        }
        
        public override void OnDisable()
        {
            base.OnDisable();
            
            _basicAI.HitAction -= UpdateRatio;
        }

        private void UpdateRatio()
        {
            DOTween.To(() => _fill, x => _fill = x, _basicAI.CurrentHp / _basicAI.MaxHp, .2f);
        }

        public override void DrawShapes(Camera cam)
        {
            using (Draw.Command(cam))
            {
                DrawHealthBar();
            }
        }

        private void OnDrawGizmosSelected()
        {
            DrawHealthBar();
        }

        private void DrawHealthBar()
        {
            Draw.Matrix = transform.localToWorldMatrix;
            var midPoint = -.5f * Mathf.PI;
            var start = midPoint + _fill * Mathf.PI;
            var end = midPoint - _fill * Mathf.PI;
            
            Draw.Arc( _radius, _thickness, start,  end, ArcEndCap.Round );
        }
    }
}