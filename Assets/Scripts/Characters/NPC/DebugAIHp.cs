using System;
using System.Globalization;
using Common.Unity;
using TMPro;
using UnityEngine;

namespace Characters.NPC
{
    public class DebugAIHp : MonoBehaviour
    {
        [SerializeField] private BasicAI _basicAI;
        [SerializeField] private TMP_Text _text;

        private void Update()
        {
            _text.text = ((int)_basicAI.CurrentHp).ToString(CultureInfo.CurrentCulture);
        }
    }
}
