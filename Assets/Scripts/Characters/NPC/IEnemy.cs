﻿using Environment;
using ScriptableObjects.Characters.NPC;

namespace Characters.NPC
{
    public interface IEnemy
    {
        void SetStats(EnemyStatsSO stats);
        void RegisterLevel(Level level);
    }
}