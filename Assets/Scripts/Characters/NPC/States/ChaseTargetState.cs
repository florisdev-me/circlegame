﻿using Common.FiniteStateMachine;
using UnityEngine;
using UnityEngine.AI;

namespace Characters.NPC.States
{
    public class ChaseTargetState : MonobehaviourState<BasicAI>
    {
        private NavMeshAgent _agent;

        public ChaseTargetState(BasicAI owner) : base(owner)
        {
            _agent = owner.Agent;
        }

        public override void Enter()
        {
        }

        public override void Exit()
        {
            if (_agent.isActiveAndEnabled)
            {
                _agent.ResetPath();
            }
        }

        public override void FixedUpdate()
        {
            if (owner.Target == null || owner.TargetTransform == null)
            {
                owner.StateMachine.ChangeState(BasicAIStates.Idle);
                return;
            }
            
            var distToTarget = Vector3.Distance(owner.TargetTransform.position, transform.position);
            if (distToTarget > owner.Stats.LooseTargetRange)
            {
                owner.StateMachine?.ChangeState(BasicAIStates.Idle);
                return;
            }

            if (distToTarget < owner.Stats.AttackRange)
            {
                owner.StateMachine?.ChangeState(BasicAIStates.Attack);
                return;
            }

            _agent.SetDestination(owner.TargetTransform.position);
        }
    }
}