﻿using Common.FiniteStateMachine;
using DG.Tweening;
using UnityEngine;

namespace Characters.NPC.States
{
    public class AttackTargetState : MonobehaviourState<BasicAI>
    {
        private float _startingScale = 1;
        private bool _attacking = false;
        
        public AttackTargetState(BasicAI owner) : base(owner)
        {
        }

        public override void Enter()
        {
            _startingScale = transform.localScale.x;
            _attacking = false;
        }

        public override void Exit()
        {
            _attacking = false;

            transform.DORewind();
            transform.DOKill();
        }

        public override void FixedUpdate()
        {
            if (_attacking)
                return;
            
            if (owner.Target == null || owner.TargetTransform == null)
            {
                owner.StateMachine.ChangeState(BasicAIStates.Idle);
                return;
            }
            
            if (Vector3.Distance(owner.TargetTransform.position, transform.position) > owner.Stats.AttackRange)
            {
                owner.StateMachine.ChangeState(BasicAIStates.Chase);
                return;
            }
            
            if (!(owner.AttackCooldown <= 0)) return;
            
            Attack();
        }

        private void Attack()
        {
            _attacking = true;

            transform.DOScale(_startingScale * 1.5f, .5f).OnComplete(
                () => transform.DOScale(_startingScale, .1f).OnComplete(FinishAttack)
            );
        }

        private void FinishAttack()
        {
            owner.Attack();
            _attacking = false;
        }
    }
}