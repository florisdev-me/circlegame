using Common.FiniteStateMachine;
using Common.Unity;
using Objectives;
using UnityEngine;

namespace Characters.NPC.States
{
    public class IdleState : MonobehaviourState<BasicAI>
    {
        public IdleState(BasicAI owner) : base(owner)
        {
        }

        public override void FixedUpdate()
        {
            var target =
                Physics2D.OverlapCircle(transform.position, owner.Stats.DetectionRange,
                    owner.Stats.ObjectiveLayer);

            if (target == null) return;

            if (!target.gameObject.TryGetInterface<ITarget>(out var targetInterface)) return;
            
            owner.SetTarget(targetInterface);
            owner.StateMachine.ChangeState(BasicAIStates.Chase);
        }
    }
}