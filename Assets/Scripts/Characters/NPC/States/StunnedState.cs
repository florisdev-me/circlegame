﻿using System.Net;
using Combat;
using Common.FiniteStateMachine;
using UnityEngine;
using UnityEngine.AI;

namespace Characters.NPC.States
{
    public class StunnedState : BaseState<BasicAI>
    {
        private Rigidbody2D _rigidbody;
        private Collider2D _collider;
        private NavMeshAgent _agent;

        private float _knockBackEffect;
        private float _stunnedTimer;
        private Vector2 _direction;
        private float _drag;
        
        public StunnedState(BasicAI owner) : base(owner)
        {
            _agent = owner.Agent;
            _rigidbody = owner.GetComponent<Rigidbody2D>();
            _collider = owner.GetComponent<Collider2D>();
        }

        public override void Enter()
        {
            if (_agent.isActiveAndEnabled)
            {
                _agent.ResetPath();
                _agent.enabled = false;
            }
            
            var hitInfo = owner.LastHitInfo;
            _knockBackEffect = hitInfo.KnockBack;
            _stunnedTimer = hitInfo.StunDuration;
            _direction = hitInfo.Direction;
            _drag = owner.Stats.Drag;
            _rigidbody.isKinematic = false;
        }

        public override void Exit()
        {
            _rigidbody.velocity = Vector2.zero;
            _agent.enabled = true;
            _rigidbody.isKinematic = true;
        }

        public override void FixedUpdate()
        {
            if (_stunnedTimer <= 0)
            {
                owner.StateMachine.ChangeState(BasicAIStates.Idle);
            }

            _stunnedTimer -= Time.fixedDeltaTime;
            _knockBackEffect = Mathf.Lerp(_knockBackEffect, 0, _drag * Time.fixedDeltaTime);
            
            // Move logic
            var targetPosition = CalculateMove(_rigidbody.position);
            _rigidbody.MovePosition(targetPosition);
        }
        
        private Vector2 CalculateMove(Vector2 sourcePosition)
        {
            return sourcePosition + _direction * (_knockBackEffect * Time.fixedDeltaTime);
        }
    }
}