using System;
using System.Collections.Generic;
using Characters.NPC.States;
using Combat;
using Common.FiniteStateMachine;
using Common.Unity;
using Common.Unity.GameManager;
using Environment;
using GameSetup;
using Objectives;
using ScriptableObjects.Characters.NPC;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

namespace Characters.NPC
{
    public enum BasicAIStates
    {
        None,
        Idle,
        Chase,
        Attack,
        Stunned
    }

    public class BasicAI : BetterMonoBehaviour, IDamageable, IEnemy
    {
        // Generic enemy setup
        public EnemyStatsSO Stats { get; private set; }

        public FiniteStateMachine<BasicAIStates, BasicAI> StateMachine { get; private set; }
        private Level _level;

        // Combat
        public ITarget Target { get; private set; }
        public Transform TargetTransform { get; private set; }
        public float AttackCooldown { get; private set; }

        // Taking damage
        public UnityAction HitAction = delegate { };
        public HitInfo LastHitInfo { get; private set; }
        public float CurrentHp { get; private set; } = 2;
        public float MaxHp => Stats.MaxHp;

        // Movement
        public NavMeshAgent Agent { get; private set; }


        private void OnEnable()
        {
            Agent = GetComponent<NavMeshAgent>();
            
            InitializeStateMachine();

            AttackCooldown = 0;
        }

        protected override void InitInvokers()
        {
            StateMachine.ChangeState(BasicAIStates.Idle);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            StateMachine.Cleanup();
        }

        private void InitializeStateMachine()
        {
            var stateDict = new Dictionary<BasicAIStates, BaseState<BasicAI>>
            {
                { BasicAIStates.Idle, new IdleState(this) },
                { BasicAIStates.Chase, new ChaseTargetState(this) },
                { BasicAIStates.Attack, new AttackTargetState(this) },
                { BasicAIStates.Stunned, new StunnedState(this) }
            };

            StateMachine = new FiniteStateMachine<BasicAIStates, BasicAI>(stateDict);
        }

        private void FixedUpdate()
        {
            if (IsPaused)
            {
                return;
            }

            if (AttackCooldown > 0)
            {
                AttackCooldown -= Time.fixedDeltaTime;
            }

            StateMachine?.FixedUpdate();
        }

        public void SetTarget(ITarget target)
        {
            Target = target;
            TargetTransform = target.GetTransform();
        }

        public void Attack()
        {
            AttackCooldown = Stats.AttackCooldown;
            Target.GetHit();
        }

        public void TakeHit(HitInfo hitInfo)
        {
            CurrentHp -= hitInfo.Damage;

            if (CurrentHp > 0)
            {
                LastHitInfo = hitInfo;
                StateMachine.ChangeState(BasicAIStates.Stunned);
                HitAction.Invoke();
            }
            else
            {
                OnDisable();
                Destroy(gameObject);
                _level.EnemyDied();
            }
        }

        protected override void TogglePause()
        {
            base.TogglePause();
            
            if (IsPaused && Agent.isActiveAndEnabled)
            {
                Agent.ResetPath();
            }
        }

        public void SetStats(EnemyStatsSO stats)
        {
            Stats = stats;
            CurrentHp = Stats.MaxHp;
            Agent.speed = Stats.Speed;
        }

        public void RegisterLevel(Level level)
        {
            _level = level;
        }
    }
}