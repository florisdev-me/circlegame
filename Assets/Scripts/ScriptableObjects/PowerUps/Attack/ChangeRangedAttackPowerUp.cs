﻿using Characters.Player.Components;
using ScriptableObjects.GameSetup.PowerUps;
using UnityEngine;

namespace ScriptableObjects.PowerUps.Attack
{
    [CreateAssetMenu(fileName = "ChangeRangedAttack", menuName = "PowerUps/Player/ChangeRangedAttack")]
    public class ChangeRangedAttackPowerUp : APowerUpSO
    {
        [Header("RangedAttack PowerUp Properties")]
        [SerializeField] private GameObject _attack;
        
        public override void ApplyPowerUp(PowerUpManagerSO manager)
        {
            // PlayerRangedAttackComponent component;
            if (!manager.PlayerPowerUpManagerSO.Player.TryGetComponent<PlayerRangedAttackComponent>(out var component))
            {
                component = manager.PlayerPowerUpManagerSO.Player.gameObject.AddComponent<PlayerRangedAttackComponent>();
            }

            component.SetAttack(_attack);
        }
    }
}