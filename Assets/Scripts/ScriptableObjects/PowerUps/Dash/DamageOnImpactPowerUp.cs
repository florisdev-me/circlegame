﻿using Characters.Player;
using Characters.Player.Components;
using ScriptableObjects.GameSetup.PowerUps;
using UnityEngine;

namespace ScriptableObjects.PowerUps.Dash
{
    [CreateAssetMenu(fileName = "DamageOnImpactPowerUp", menuName = "PowerUps/Player/DamageOnImpactPowerUp")]
    public class DamageOnImpactPowerUp : APowerUpSO
    {
        [Header("DamageOnImpact PowerUp Properties")]
        [SerializeField] private PlayerStates _stateToActivate;
        
        public override void ApplyPowerUp(PowerUpManagerSO manager)
        {
            // PlayerRangedAttackComponent component;
            if (!manager.PlayerPowerUpManagerSO.Player.TryGetComponent<PlayerDamageOnImpactComponent>(out var component))
            {
                component = manager.PlayerPowerUpManagerSO.Player.gameObject.AddComponent<PlayerDamageOnImpactComponent>();
            }

            component.AddToActiveStates(_stateToActivate);
        }
    }
}