﻿using System.Collections.Generic;
using Characters.Player;
using Characters.Player.Components;
using ScriptableObjects.GameSetup;
using ScriptableObjects.GameSetup.PowerUps;
using UnityEngine;

namespace ScriptableObjects.PowerUps
{
    public enum PowerUpType
    {
        Player,
        Objective,
        Both
    }
    
    // [CreateAssetMenu(fileName = "PlayerUpgrade", menuName = "Characters/Player/Player Upgrade")]
    public abstract class APowerUpSO : ScriptableObject
    {
        [Header("Generic PowerUp Properties")]
        [SerializeField] private GameObject _pickupPrefab;
        public GameObject PickupPrefab => _pickupPrefab;
        
        [SerializeField] private Sprite _sprite;
        public Sprite Sprite => _sprite;
        
        [SerializeField] private PowerUpType _type;
        public PowerUpType Type => _type;

        [SerializeField] private List<APowerUpSO> _powerUpsUnlocks;
        public List<APowerUpSO> PowerUpsUnlocks => _powerUpsUnlocks;
        
        public abstract void ApplyPowerUp(PowerUpManagerSO manager);
    }
}