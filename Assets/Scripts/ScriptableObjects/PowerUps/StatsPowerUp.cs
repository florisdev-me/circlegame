﻿using System;
using System.Collections.Generic;
using Objectives;
using ScriptableObjects.Characters.Player;
using ScriptableObjects.GameSetup.PowerUps;
using UnityEngine;

namespace ScriptableObjects.PowerUps
{
    [Serializable]
    public struct PlayerStatPowerUp
    {
        public PlayerStatType StatType;
        public float Delta;
    }

    [Serializable]
    public struct ObjectiveStatPowerUp
    {
        public ObjectiveStatType StatType;
        public float Delta;
    }
    
    /// <summary>
    /// SO for applying stat based powerUps
    /// </summary>
    [CreateAssetMenu(fileName = "PlayerDamagePowerUp", menuName = "PowerUps/StatPowerUp")]
    public class StatsPowerUp : APowerUpSO
    {
        [Header("Stats PowerUp Properties")]
        [SerializeField] private List<PlayerStatPowerUp> _playerPowerUps;
        [SerializeField] private  List<ObjectiveStatPowerUp> _objectivePowerUps;

        public override void ApplyPowerUp(PowerUpManagerSO manager)
        {
            foreach (var playerPowerUp in _playerPowerUps)
            {
                manager.PlayerPowerUpManagerSO.ApplyStatPowerUp(playerPowerUp);
            }
            
            foreach (var objectivePowerUp in _objectivePowerUps)
            {
                manager.ObjectivePowerUpManagerSO.ApplyStatPowerUp(objectivePowerUp);
            }
        }
    }
}