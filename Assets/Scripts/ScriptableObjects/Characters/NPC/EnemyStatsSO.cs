using UnityEngine;

namespace ScriptableObjects.Characters.NPC
{
    [CreateAssetMenu(fileName = "EnemyStats", menuName = "Characters/NPC/Enemy Stats")]
    public class EnemyStatsSO : ScriptableObject
    {
        [Header("Detection")] 
        [SerializeField] private float _detectionRange = 100;
        public float DetectionRange => _detectionRange;
        [SerializeField] private float _looseTargetRange = 100;
        public float LooseTargetRange => _looseTargetRange;

        [SerializeField] private LayerMask _objectiveLayer = new LayerMask();
        public LayerMask ObjectiveLayer => _objectiveLayer;

        [Header("Combat")]
        [SerializeField] private float _attackCooldown = 1;
        public float AttackCooldown => _attackCooldown;
        [SerializeField] private float _attackRange = 100;
        public float AttackRange => _attackRange;
        [SerializeField] private float _maxHp = 2;
        public float MaxHp => _maxHp;

        [Header("Movement")] 
        [SerializeField] private float _drag = 5;
        public float Drag => _drag;
        [SerializeField] private float _speed = 1;
        public float Speed => _speed;
    }
}