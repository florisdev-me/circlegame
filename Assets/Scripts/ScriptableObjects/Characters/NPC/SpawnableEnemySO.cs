﻿using UnityEngine;

namespace ScriptableObjects.Characters.NPC
{
    [CreateAssetMenu(fileName = "Spawnable Enemy", menuName = "Characters/NPC/Spawnable Enemy")]
    public class SpawnableEnemySO : ScriptableObject
    {
        [SerializeField] private float _postSpawnDelay = .2f;
        public float PostSpawnDelay => _postSpawnDelay;
        
        [SerializeField] private GameObject _enemyPrefab;
        public GameObject EnemyPrefab => _enemyPrefab;
        
        [SerializeField] private EnemyStatsSO _stats;
        public EnemyStatsSO Stats => _stats;
    }
}