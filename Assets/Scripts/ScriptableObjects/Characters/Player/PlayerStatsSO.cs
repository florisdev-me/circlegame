using UnityEngine;

namespace ScriptableObjects.Characters.Player
{
    /// <summary>
    /// SO for setting a players initial stats
    /// </summary>
    
    [CreateAssetMenu(fileName = "PlayerStats", menuName = "Characters/Player/Player Stats")]
    public class PlayerStatsSO : ScriptableObject
    {
        [SerializeField] private float _dashBaseDamage = 3;
        public float DashBaseDamage => _dashBaseDamage;
        
        [SerializeField] private float _dashBaseStunDuration = 1;
        public float DashBaseStunDuration => _dashBaseStunDuration;
        
        [SerializeField] private float _dashBaseKnockBack = 15;
        public float DashBaseKnockBack => _dashBaseKnockBack;
        
    }
}