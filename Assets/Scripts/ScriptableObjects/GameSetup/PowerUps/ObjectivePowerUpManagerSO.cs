﻿using System;
using System.Collections.Generic;
using Objectives;
using ScriptableObjects.PowerUps;
using UnityEngine;

namespace ScriptableObjects.GameSetup.PowerUps
{
    public enum ObjectiveStatType
    {
        Health = 0,
        TurretAttackRate = 10,
        TurretAimSpeed = 11,
        TurretRange = 12,
        TurretDamage = 15,
        TurretKnockback = 20,
        TurretStun = 21,
        TurretProjectileCount = 22
    }

    [CreateAssetMenu(fileName = "Objective PowerUp Manager",
        menuName = "Game Setup/Power Ups/Objective PowerUp Manager")]
    public class ObjectivePowerUpManagerSO : ScriptableObject
    {
        public CrystalObjective Objective { get; private set; }
        private Dictionary<ObjectiveStatType, float> _powerUps;
        

        private void OnEnable()
        {
            Reset();
        }

        private void OnDisable()
        {
            Reset();
        }

        private void Reset()
        {
            _powerUps = new Dictionary<ObjectiveStatType, float>()
            {
                { ObjectiveStatType.Health, 1 }
            };
        }

        public void RegisterObjective(CrystalObjective objective)
        {
            Objective = objective;
        }

        public void ApplyStatPowerUp(ObjectiveStatPowerUp powerUp)
        {
            _powerUps[powerUp.StatType] += powerUp.Delta;
        }
        
        public float GetPowerUpValue(ObjectiveStatType statType)
        {
            return _powerUps[statType];
        }
    }
}