﻿using System.Collections.Generic;
using Characters.Player;
using ScriptableObjects.PowerUps;
using UnityEngine;

namespace ScriptableObjects.GameSetup.PowerUps
{
    public enum PlayerStatType
    {
        DamageMultiplier = 0,
        AttackRate = 10,
        ProjectileSpeed = 20,
        ProjectileCount = 21,
        ProjectileBounces = 22,
        ProjectilePiercing = 23,
        DashCooldown = 30,
        DashStack = 31,
        DashControl = 32,
        DashDistance = 33,
        DashKnockBack = 34,
        DashDamage = 35,
        DashStun = 40
    }
    
    [CreateAssetMenu(fileName = "Player PowerUp Manager", menuName = "Game Setup/Power Ups/Player PowerUp Manager")]
    public class PlayerPowerUpManagerSO : ScriptableObject
    {
        public Player Player { get; private set; }
        private Dictionary<PlayerStatType, float> _powerUps;
        private float _damageMultiplier;

        private void OnEnable()
        {
            Reset();
        }

        private void OnDisable()
        {
            Reset();
        }

        private void Reset()
        {
            _powerUps = new Dictionary<PlayerStatType, float>()
            {
                { PlayerStatType.DamageMultiplier, 1 },
                { PlayerStatType.DashDamage, 1 }
            };
        }
        
        public void RegisterPlayer(Player player)
        {
            Player = player;
        }

        public void ApplyStatPowerUp(PlayerStatPowerUp powerUp)
        {
            _powerUps[powerUp.StatType] += powerUp.Delta;
        }
        
        public float GetPowerUpValue(PlayerStatType statType)
        {
            return _powerUps[statType];
        }
    }
}