using System.Collections.Generic;
using System.Linq;
using Characters.Player.Components;
using Objectives;
using ScriptableObjects.PowerUps;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ScriptableObjects.GameSetup.PowerUps
{
    [CreateAssetMenu(fileName = "PowerUp Manager", menuName = "Game Setup/Power Ups/PowerUp Manager")]
    public class PowerUpManagerSO : ScriptableObject
    {
        [SerializeField] private List<APowerUpSO> _initialPowerUpPool;
        private List<APowerUpSO> _currentPowerUpPool;
        
        // Not currently used, but can be used to save/load already applier powerUps
        private List<APowerUpSO> _appliedPowerUps;

        [SerializeField] private PlayerPowerUpManagerSO _playerPowerUpManager;
        public PlayerPowerUpManagerSO PlayerPowerUpManagerSO => _playerPowerUpManager;

        [SerializeField] private ObjectivePowerUpManagerSO _objectivePowerUpManager;
        public ObjectivePowerUpManagerSO ObjectivePowerUpManagerSO => _objectivePowerUpManager;

        private void OnEnable()
        {
            _appliedPowerUps = new List<APowerUpSO>();
            _currentPowerUpPool = new List<APowerUpSO>();

            foreach (var powerUp in _initialPowerUpPool)
            {
                _currentPowerUpPool.Add(powerUp);
            }
        }

        public List<APowerUpSO> GetPotentialPowerUps(int count)
        {
            return _currentPowerUpPool.OrderBy(_ => Random.Range(0f, 1f)).Take(count).ToList();
        }

        public void ApplyPowerUp(APowerUpSO powerUp)
        {
            powerUp.ApplyPowerUp(this);
            _appliedPowerUps.Add(powerUp);

            _currentPowerUpPool.Remove(powerUp);
            foreach (var unlock in powerUp.PowerUpsUnlocks)
            {
                _currentPowerUpPool.Add(unlock);
            }
        }
    }
}