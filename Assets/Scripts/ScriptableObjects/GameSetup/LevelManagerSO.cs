using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common.Unity;
using Environment;
using ScriptableObjects.Environment;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace ScriptableObjects.GameSetup
{
    [CreateAssetMenu(fileName = "LevelManager", menuName = "Game Setup/Level Manager")]
    public class LevelManagerSO : ScriptableObject
    {
        [SerializeField] private List<LevelSO> _levelCandidates;
        public UnityAction<Vector2> TransitionPlayerSpawn = delegate { };
        private Level _currentLevel;

        private bool _levelInitialized;
        private bool _playerSpawned;

        public void LoadInitialLevel(Action onComplete)
        {
            var level = _levelCandidates.OrderBy(_ => Random.Range(0f, 1f)).First();
            SpawnLevel(level);
            UnityHelper.Instance.StartCoroutine(LoadLevel(onComplete));
        }

        public void StartNextLevel(LevelSO nextLevel, Action onComplete)
        {
            SpawnLevel(nextLevel);
            UnityHelper.Instance.StartCoroutine(LoadLevel(onComplete));
        }

        private void SpawnLevel(LevelSO levelSO)
        {
            // Destroy previous level
            if (_currentLevel != null)
            {
                _currentLevel.enabled = false;
            }

            // Load in new level
            _levelInitialized = false;
            _playerSpawned = false;
            var instantiated = Instantiate(levelSO.LevelPrefab);
            if (!instantiated.TryGetComponent<Level>(out var level))
            {
                Debug.LogError(
                    "RoomSO prefab has no Room script attached to the parent. Loading scene could not be completed.");
            }

            _currentLevel = level;
            level.InitializeLevel(() => _levelInitialized = true, TransitionPlayerSpawn);
        }

        public List<LevelSO> GetLevelCandidates()
        {
            return _levelCandidates;
        }

        public void PlayerFinishedMoving()
        {
            _playerSpawned = true;
        }

        private IEnumerator LoadLevel(Action onComplete)
        {
            while (!_playerSpawned || !_levelInitialized)
            {
                yield return null;
            }
            
            onComplete();
        }
    }
}