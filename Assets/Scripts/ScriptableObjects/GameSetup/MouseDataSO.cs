using Common.Math;
using UnityEngine;

namespace ScriptableObjects.GameSetup
{
    [CreateAssetMenu(fileName = "MouseData", menuName = "Game Setup/Mouse Data")]
    public class MouseDataSO : ScriptableObject
    {
        public Vector2 ScreenPosition = Vector2.zero;
        public Vector3 WorldPosition => MouseMath.MouseWorldPosition2D(ScreenPosition);
    }
}
