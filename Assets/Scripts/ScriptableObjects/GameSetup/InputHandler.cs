using System;
using Generated;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace ScriptableObjects.GameSetup
{
    [CreateAssetMenu(fileName = "InputHandler", menuName = "Game Setup/Input Handler")]
    public class InputHandler : ScriptableObject, GameInput.IInGameBindingsActions
    {
        public event UnityAction<Vector2> MoveEvent = delegate { };
        public event UnityAction<bool> DashEvent = delegate { };
        public event UnityAction<bool> FireEvent = delegate { };
        
        private GameInput _gameInput;
        [SerializeField] private MouseDataSO _mouseData;

        private void OnEnable()
        {
            if (_gameInput == null)
            {
                _gameInput = new GameInput();
                _gameInput.InGameBindings.SetCallbacks(this);
            }
            
            // Enable desire input scheme
            _gameInput.InGameBindings.Enable();
        }

        private void OnDisable()
        {
            // Disable all input schemes
            _gameInput?.InGameBindings.Disable();
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            MoveEvent.Invoke(context.ReadValue<Vector2>());
        }

        public void OnMouseMove(InputAction.CallbackContext context)
        {
            if (_mouseData == null)
            {
                Debug.LogError("Mouse data was not assigned to the InputHandler");
                return;
            }
            
            _mouseData.ScreenPosition = context.ReadValue<Vector2>();
        }

        public void OnDash(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                DashEvent.Invoke(true);
            }
            else if (context.canceled)
            {
                DashEvent.Invoke(false);
            }
        }

        public void OnFire(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                FireEvent.Invoke(true);
            }
            else if (context.canceled)
            {
                FireEvent.Invoke(false);
            }
        }
    }
}
