﻿using System.Collections.Generic;
using ScriptableObjects.Characters.NPC;
using UnityEngine;

namespace ScriptableObjects.Environment
{
    [CreateAssetMenu(fileName = "Enemy Wave", menuName = "Environment/EnemyWave")]
    public class EnemyWaveSO : ScriptableObject
    {
        [SerializeField] private List<SpawnableEnemySO> _enemiesToSpawn;
        public List<SpawnableEnemySO> EnemiesToSpawn => _enemiesToSpawn;

        public int GetEnemyCount()
        {
            return _enemiesToSpawn.Count;
        }
    }
}