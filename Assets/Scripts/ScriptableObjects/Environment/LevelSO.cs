﻿using UnityEngine;

namespace ScriptableObjects.Environment
{
    [CreateAssetMenu(fileName = "Level", menuName = "Environment/Level")]
    public class LevelSO : ScriptableObject
    {
        [SerializeField] private GameObject _levelPrefab;
        public GameObject LevelPrefab => _levelPrefab;

        [SerializeField] private string _name;
        public string Name => _name;

        [SerializeField] private Sprite _sprite;
        public Sprite Sprite => _sprite;
    }
}