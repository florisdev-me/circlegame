using System;
using System.Globalization;
using Common.Unity;
using Common.Unity.GameManager;
using DG.Tweening;
using ScriptableObjects.GameSetup.PowerUps;
using UnityEngine;
using UnityEngine.Events;

namespace Objectives
{
    public class CrystalObjective : BetterMonoBehaviour, ITarget
    {
        [SerializeField] private UnityEvent<string> HealthUpdate;
        [SerializeField] private float _maxHealth = 25;

        private ObjectivePowerUpManagerSO _powerUpManager;
        private float _currentHp;

        private float _startingScale;

        private void OnEnable()
        {
            _startingScale = transform.localScale.x;
        }

        protected override void InitReferences()
        {
            base.InitReferences();

            _powerUpManager = gameLibrary.PowerUpManager.ObjectivePowerUpManagerSO;
            _powerUpManager.RegisterObjective(this);

            _maxHealth = _powerUpManager.GetPowerUpValue(ObjectiveStatType.Health) * _maxHealth;
            _currentHp = _maxHealth;
        }

        protected override void InitInvokers()
        {
            HealthUpdate.Invoke(_currentHp.ToString(CultureInfo.InvariantCulture));
        }

        public Transform GetTransform()
        {
            return transform;
        }

        public void GetHit()
        {
            transform.DORestart();
            transform.DOScale(_startingScale * .75f, .2f).OnComplete(
                () => transform.DOScale(_startingScale, .5f)
            );

            _currentHp--;
            HealthUpdate.Invoke(((int)_currentHp).ToString(CultureInfo.InvariantCulture));

            if (_currentHp <= 0)
            {
                Destroy(gameObject);
                gameManager.GameOver();
            }
        }
    }
}