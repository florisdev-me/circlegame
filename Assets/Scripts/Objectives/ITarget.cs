using UnityEngine;

namespace Objectives
{
    public interface ITarget
    {
        public Transform GetTransform();
        public void GetHit();
    }
}
